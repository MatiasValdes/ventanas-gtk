#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk


# Creamos una clase que contenga solo la pantalla de informacion de eliminar
# para un mejor manejo
class dlgEliminar():
    def __init__(self):
        self.builder = Gtk.Builder()
        self.builder.add_from_file("./ui/ejemplo.ui")
        # Construimos la ventana de dialogo
        self.ventana = self.builder.get_object("dlgEliminar")
        self.ventana.set_title("¡Cuidado!")
        self.ventana.resize(600, 400)
        self.ventana.set_property("text",
                                  "¿Seguro que desea eliminar este nombre?")
        # Creacion del boton cancelar por codigo y no por glade
        boton_cancelar = self.ventana.add_button(Gtk.STOCK_CANCEL,
                                                 Gtk.ResponseType.CANCEL)
        boton_cancelar.set_always_show_image(True)
        boton_cancelar.connect("clicked", self.boton_cancelar_clicked)

        # Creacion boton aceptar
        boton_aceptar = self.ventana.add_button(Gtk.STOCK_OK,
                                                Gtk.ResponseType.OK)
        boton_aceptar.set_always_show_image(True)
        boton_aceptar.connect("clicked", self.boton_aceptar_clicked)

    # Metodo en el cual al apretar cancelar se cierra el dialogo al igual
    # que con el de aceptar porque el metodo de borrado esta despues
    def boton_cancelar_clicked(self, btn=None):
        self.ventana.destroy()

    # La respuesta de tipo ok está predefinida en el main por lo tanto
    # al ejecutar el botón de tipo ok, está llegando la señal al main
    def boton_aceptar_clicked(self, btn=None):
        self.ventana.destroy()
