#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk


class dlgPrincipal():
    def __init__(self, titulo=""):
        self.builder = Gtk.Builder()
        self.builder.add_from_file("./ui/ejemplo.ui")
        # Ventana
        self.dialogo = self.builder.get_object("dlgEjemplo")
        self.dialogo.set_title(titulo)
        self.dialogo.resize(600, 400)

        # boton cancelar
        boton_cancelar = self.dialogo.add_button(Gtk.STOCK_CANCEL,
                                                 Gtk.ResponseType.CANCEL)
        boton_cancelar.set_always_show_image(True)
        boton_cancelar.connect("clicked", self.boton_cancelar_clicked)

        # boton aceptar
        boton_aceptar = self.dialogo.add_button(Gtk.STOCK_OK,
                                                Gtk.ResponseType.OK)
        boton_aceptar.set_always_show_image(True)
        boton_aceptar.connect("clicked", self.boton_aceptar_clicked)

        # cuadros de texto
        self.nombre = self.builder.get_object("nombre")
        self.apellido = self.builder.get_object("apellido")

    def boton_cancelar_clicked(self, btn=None):
        self.dialogo.destroy()

    # Metodo que permite entregar un texto de enhorabuena al ingresar datos
    def boton_aceptar_clicked(self, btn=None):
        self.adv = self.builder.get_object("dlgAdvertencia")
        nombre = self.nombre.get_text()
        apellido = self.apellido.get_text()
        if nombre is "" or apellido is "":
            # Le colocamos un tamaño a la ventana e ingresamos los datos
            self.adv.resize(600, 400)
            self.adv.set_property("text",
                                  "¡PRECAUCION!")
            self.adv.set_property("secondary_text", "Los datos ingresados no"
                                  " pueden ser vacíos")
            response = self.adv.run()

        else:
            # Le colocamos un tamaño a la ventana e ingresamos los datos
            self.adv.resize(600, 400)
            self.adv.set_property("text", "¡Enhorabuena!")
            self.adv.set_property("secondary_text", "Los datos ingresados han"
                                  " sido guardados correctamente")
            response = self.adv.run()
        if response == Gtk.ResponseType.OK:
            self.adv.destroy()
            return
        return

    
